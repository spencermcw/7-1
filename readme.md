# 7-1 Pattern Generator

> Generates a generic 7-1 SASS structure in a new directory named 'style'

For use with [npm](https://www.npmjs.com/package/7-1)

`$ 7-1 init`
